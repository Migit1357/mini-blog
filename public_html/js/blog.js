$(function () {
    var APPLICATION_ID = "0BDF281F-9659-CEA7-FF4E-6E5AF218E700",
        SECRECT_KEY = "1C9A7CA6-64D7-3D29-FFE1-84E4C260E300",
        VERSION = "v1";
    
    Backendless.initApp(APPLICATION_ID, SECRECT_KEY, VERSION);
    
    var postsCollection = Backendless.Persistence.of(Posts).find();

    console.log(postsCollection);
    
    var wrapper = {
        posts: postsCollection.data
    };
    
    Handlebars.registerHelper("format", function (time) {
        return moment(time).format("dddd, MMMM Do YYYY");
    });
    
    var blogScript = $("#blogs-template").html();
    var blogTemplate = Handlebars.compile(blogScript);
    var blogHTML = blogTemplate(wrapper);
    
    $('.main-container').html(blogHTML);
});

function Posts(args) {
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
    
}

