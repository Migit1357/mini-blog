$(function () {
    var APPLICATION_ID = "0BDF281F-9659-CEA7-FF4E-6E5AF218E700",
        SECRECT_KEY = "1C9A7CA6-64D7-3D29-FFE1-84E4C260E300",
        VERSION = "v1";
    
    Backendless.initApp(APPLICATION_ID, SECRECT_KEY, VERSION);
    
    var loginScript = $("#login-template").html();
    var loginTemplate = Handlebars.compile(loginScript);
    
    $('.main-container').html(loginTemplate);
    
    $(document).on('submit', '.form-sigin', function(event){
        event.preventDefault();
        
        var data = $(this).serializeArray(),
        email = data[0].value,
        password = data[1].value;
        
        Backendlesss.UserServicce.login(email, password, true, new Backendless.Async(userLoggedIn, gotError));
    });
});

function Posts(args) {
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
    
}

function userLoggedIn() {
    console.log("user successfully logged in");
}

function gotError(error) {
    console.log("Error message - " + error.message);
    console.log("Error code - " + error.code)
}
